package com.kyrychenko;

public class FunctionalInterfaceTest {
    public static void main(String[] args) {
        ThreeFunction max = (a, b, c) -> Math.max(Math.max(a,b), c);
        ThreeFunction average = (a, b, c) -> (a + b + c) / 3;
        System.out.println(max.accept(1,3,6));
        System.out.println(average.accept(1,3,6));
    }
}
