package com.kyrychenko.application;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

public class Main {
    public static void main(String[] args) {
        String s = "";
        Scanner sc = new Scanner(System.in);
        List<String> list = new ArrayList<>();
        do{
            s = sc.nextLine();
            if(!s.isEmpty()){
                list.add(s);
            }
        } while(!s.isEmpty());

        System.out.println(list.stream()
                .map(line -> Arrays.asList(line.split(" ")))
                .flatMap(l -> l.stream())
                .distinct()
                .count());
        System.out.println(list.stream()
                .map(line -> Arrays.asList(line.split(" ")))
                .flatMap(l -> l.stream())
                .distinct()
                .sorted()
                .collect(Collectors.toList()));
        System.out.println(list.stream()
                .map(line -> Arrays.asList(line.split(" ")))
                .flatMap(l -> l.stream())
                .collect(Collectors.groupingBy(a -> a, Collectors.summingInt(a -> 1))));
        System.out.println(list.stream()
                .map(line -> Arrays.asList(line.split(" ")))
                .flatMap(l -> l.stream())
                .map(line -> Arrays.asList(line.split("")))
                .flatMap(l -> l.stream())
                .filter(v -> v.toLowerCase() == v)
                .collect(Collectors.groupingBy(a -> a, Collectors.summingInt(a -> 1))));
    }
}
