package com.kyrychenko.list;

import java.util.ArrayList;
import java.util.List;
import java.util.OptionalDouble;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class TestStreams {
    public static void main(String[] args) {
        System.out.println(generate1().stream().mapToInt(a -> (int)a).sum());
        System.out.println(generate2().stream().reduce(0, (a, b) -> a + b));
        System.out.println(generate1().stream().mapToInt(a -> (int)a).max());
        System.out.println(generate2().stream().mapToInt(a -> (int)a).min());
        System.out.println(generate1().stream().mapToInt(a -> (int)a).average());
        double b = generate1().stream().mapToInt(a -> (int)a).average().getAsDouble();
        System.out.println(generate1().stream().mapToInt(a -> (int)a).filter(a -> a > b).count());
    }

    static List<Integer> generate1(){
       return Stream.generate( () -> (new Random()).nextInt(100)).limit(20).collect(Collectors.toList());
    }

    static List<Integer> generate2(){
        return Stream.iterate(0, n -> n + 2).limit(20).collect(Collectors.toList());
    }
}
