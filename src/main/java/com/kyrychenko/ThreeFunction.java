package com.kyrychenko;

@FunctionalInterface
public interface ThreeFunction {
    int accept(int a, int b, int c);
}
