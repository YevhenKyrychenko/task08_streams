package com.kyrychenko.command;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class RadioTest {
    public static void main(String[] args) {
        String s = "";
        Scanner sc = new Scanner(System.in);
        Radio myRadio = new Radio();
        RadioCommandExecutor executor = new RadioCommandExecutor();
        Map<String, RadioCommand> map = new HashMap<>();
        map.put("on", new TurnOnRadioCommand(myRadio));
        map.put("off", new RadioCommand() {
            @Override
            public String execute() {
                return myRadio.off();
            }
        });
        map.put("+", () -> myRadio.addVolume());
        map.put("-", myRadio::reduceVolume);
        do{
            s = sc.nextLine();
            if(!s.equals("0")) {
                System.out.println(executor.executeCommand(map.get(s)));
            }
        } while (!s.equals("0"));
    }
}
