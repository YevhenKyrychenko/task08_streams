package com.kyrychenko.command;

public class TurnOnRadioCommand implements RadioCommand {

    private Radio radio;

    public TurnOnRadioCommand(Radio radio) {
        this.radio = radio;
    }

    @Override
    public String execute() {
        return this.radio.on();
    }
}
