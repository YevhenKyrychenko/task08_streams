package com.kyrychenko.command;

public class RadioCommandExecutor {
    public String executeCommand(RadioCommand radioCommand) {
        return radioCommand.execute();
    }
}
