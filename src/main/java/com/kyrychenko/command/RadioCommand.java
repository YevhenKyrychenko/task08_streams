package com.kyrychenko.command;

@FunctionalInterface
public interface RadioCommand {
    String execute();
}
