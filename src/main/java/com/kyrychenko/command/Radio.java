package com.kyrychenko.command;

public class Radio {
    public String on() {
        return "Radio is on";
    }

    public String off() {
        return "Radio is off";
    }

    public String addVolume() {
        return "Radio is adding volume";
    }

    public String reduceVolume() {
        return "Radio is reducing volume";
    }
}
